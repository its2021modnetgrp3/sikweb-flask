#!/bin/sh
adduser -D devel && echo 'devel:$1$nEx7osHr$waz35SiskP6jd2lPv5.hs.' | chpasswd -e
adduser -D test && echo 'test:test' | chpasswd

apk update && apk upgrade && apk add openssh-sftp-server openssh-client dropbear && rm -rf /var/cache/apk/*
mkdir /etc/dropbear
touch /var/log/lastlog

mkdir /something
cp /etc/passwd /something
cp /etc/shadow /something
cd /something
tar -cvzf backup.tar.gz *
mkdir /backup
mv backup.tar.gz /backup
cd /
rm -rf /something
chmod 777 /backup
cd /app

dropbear -R -p 22

python ImageSharing.py